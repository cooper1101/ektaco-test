App was created with create-react-app
To run the project use npm start or yarn start.

This is a test task for Ektaco.

Small overview of the app and workflow:

1. App consists of the main entry point component App (left as default from create-react-app). App contains a small header
and Routes.

2. There are just two routes in the app, the main home route handles getting the user list and the second route takes
the index of the user inside the redux store and takes to the edit user page. Both routes have a container (smart component)
and a few view components (dumb). Routes are hard-coded as this is only a demo app. On a bigger project it makes sense to
push routes into the redux store also using react-router-redux and use ConnectedRouter to pass link the router to history.
Routes are not localized (translatable) here as this is a test task and this was not in the initial spec.

3. The redux store utilizes the redux-saga library to use watch function when the get users event is published so it can then
fetch the users and push them to state upon receiving. Inside the store user array element index is used as an id as the API
does not provide any id by default. Reducer is combined through rootReducer wrapper which later is passed to createStore method
and saga is passed into rootSaga to aggregate all sagas and later pass them to middleware. This may be a little overkill 
for such a small app, but it's easy to add more sagas and reducers following the set logic.

4. For views mostly bootstrap was used and only 2 custom css files as there was not much styles to be added. Ideally each
 view component should have it's own css (or better less or scss) file. For user edit no input validation was used as that was not
 in the spec and due to the fact that the API itself could provide entries with empty fields. Again, ideally inputs should
  also be validated (both on FE and BE).
  
5. Tests. This part is really simple and covers a few components and reducer part as I am not YET used to writing them. Still 
learning this part (particularly how to mock in containers). This might have been the hardest part of the task.

If something else comes to mind, this readme may be updated with a check-in.