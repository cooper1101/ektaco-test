export const API_URL_BASE = "http://uinames.com/api/";
export const GET_USER_AMOUNT = 100;
export const LOADER_COLOR = "#FAFAFA";
export const LOADER_SIZE = 150;
export const ALLOWED_GENDERS = [
    'male', 'female', 'other', 'social justice warrior'
];