import renderer from 'react-test-renderer';
import React from "react";
import {EditUserForm} from '../views/components/edituser.form';
import {SuccessMessage} from "../views/components/edituser.success";

describe('Renders the user edit form', () => {
    it('user edit form dumb component matches the snapshot', function () {
        const userDataMock = {name: "Pete", surname: "Willson", gender: "male", region: "North Pole"};

        const component = renderer.create(
            <EditUserForm
            userData={userDataMock}
            onInputChange={jest.fn()}
            onFormSubmit={jest.fn()}
        />);

        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('shows success message if isSuccess in store is true', () => {
        const isSuccess = true;

        const component = renderer.create(
            <SuccessMessage
                isSuccess={isSuccess}
            />
        );

        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('does not show the massage if isSuccess in store is false', () => {
        const isSuccess = false;

        const component = renderer.create(
            <SuccessMessage
                isSuccess={isSuccess}
            />
        );

        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    })
});

