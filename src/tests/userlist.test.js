import {UserTable} from "../views/components/userlist.table";
import UserListContainer from '../views/userlist.container';
import React from "react";
import {SpinnerLoader} from "../views/components/spinner.loader";
import renderer from 'react-test-renderer';

const userListMock = [{
    name: 'Any',
    surname: 'Juan Carlos Sánchez',
    gender: 'female',
    region: 'North Pole'
}, {
    name: 'Giovana',
    surname: 'Montes',
    gender: 'female',
    region: 'Brazil'
}, {
    name: 'Ηρόδοτος',
    surname: 'Κομνηνός',
    gender: 'male',
    region: 'Greece'
}];

describe('Renders the user list', () => {
    it('renders users table with provided data', () =>{
        const component = renderer.create(
            <UserTable
                users={userListMock}
                onUserClick={jest.fn()}
                onUserDelete={jest.fn()}
            />
        );

        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('renders loading spinner', () =>{
        const isLoading = true;

        const component = renderer.create(
            <SpinnerLoader isLoading={isLoading}/>
        );

        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
});