import {rootReducer as reducer} from "../reducers/index.reducer";

describe('user reducer', () => {
    it('should start to get users', () => {
        let state;
        state = reducer({users: {usersList: [], getSuccess: true, isLoading: false}}, {type: 'GET_USERS'});
        expect(state).toEqual({users: {usersList: [], getSuccess: true, isLoading: true}});
    });

    it('should receive users', () => {
        let state;
        state = reducer({users: {usersList: [], getSuccess: true, isLoading: true}}, {
            type: 'USERS_RECEIVED',
            payload: [{
                name: 'Mathilde',
                surname: 'Payet',
                gender: 'female',
                region: 'France'}]
        });
        expect(state).toEqual({
            users: {
                usersList: [{
                    name: 'Mathilde',
                    surname: 'Payet',
                    gender: 'female',
                    region: 'France'
                }], getSuccess: true, isLoading: false
            }
        });
    });

    it('should delete a user', () => {
        let state;
        state = reducer({
            users: {
                usersList: [{
                    name: 'Mathilde',
                    surname: 'Payet',
                    gender: 'female',
                    region: 'France'
                }, {
                    name: '',
                    surname: 'Juan Carlos Sánchez',
                    gender: 'male',
                    region: 'Colombia'
                }, {
                    name: 'Giovana',
                    surname: 'Montes',
                    gender: 'female',
                    region: 'Brazil'
                }], getSuccess: true, isLoading: false
            }
        }, {type: 'USER_DELETE', payload: {index: 1}});
        expect(state).toEqual({
            users: {
                usersList: [{
                    name: 'Mathilde',
                    surname: 'Payet',
                    gender: 'female',
                    region: 'France'
                }, {
                    name: 'Giovana',
                    surname: 'Montes',
                    gender: 'female',
                    region: 'Brazil'
                }], getSuccess: true, isLoading: false
            }
        });
    });

    it('should edit a user', () => {
        let state;
        state = reducer({
            users: {
                usersList: [{
                    name: '',
                    surname: 'Juan Carlos Sánchez',
                    gender: 'male',
                    region: 'Colombia'
                }, {
                    name: 'Giovana',
                    surname: 'Montes',
                    gender: 'female',
                    region: 'Brazil'
                }, {
                    name: 'Ηρόδοτος',
                    surname: 'Κομνηνός',
                    gender: 'male',
                    region: 'Greece'
                }], getSuccess: true, isLoading: false
            }
        }, {
            type: 'USER_MODIFIED',
            payload: {
                index: 0,
                user: {name: 'Any', surname: 'Juan Carlos Sánchez', gender: 'female', region: 'North Pole'}
            }
        });
        expect(state).toEqual({
            users: {
                usersList: [{
                    name: 'Any',
                    surname: 'Juan Carlos Sánchez',
                    gender: 'female',
                    region: 'North Pole'
                }, {
                    name: 'Giovana',
                    surname: 'Montes',
                    gender: 'female',
                    region: 'Brazil'
                }, {
                    name: 'Ηρόδοτος',
                    surname: 'Κομνηνός',
                    gender: 'male',
                    region: 'Greece'
                }], getSuccess: true, isLoading: false
            }
        });
    });
});

