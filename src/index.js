import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './app';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from "react-redux";
import {applicationStore} from "./index.config";
import {BrowserRouter} from 'react-router-dom';

const root = document.getElementById('root');

ReactDOM.render(
    <Provider store={applicationStore}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    root);

registerServiceWorker();
