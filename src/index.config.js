import createStore from "redux/src/createStore";
import {rootReducer} from "./reducers/index.reducer";
import {composeWithDevTools} from "redux-devtools-extension";
import {applyMiddleware} from "redux";
import createSagaMiddleware from "redux-saga";
import {rootSaga} from "./sagas/index.sagas";

const sagaMiddleware = createSagaMiddleware();

let applicationStoreCreator;

//TEST or Staging builds
if (process.env.NODE_ENV !== 'production'){
    applicationStoreCreator = createStore(
        rootReducer,
        composeWithDevTools(
            applyMiddleware(
                sagaMiddleware
            )
        )
    );
}
else
{
    applicationStoreCreator = createStore(
        rootReducer,
        applyMiddleware(
            sagaMiddleware
        )
    );
}

//global Redux store with middleware application store
export const applicationStore = applicationStoreCreator;
sagaMiddleware.run(rootSaga);