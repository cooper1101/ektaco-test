import {
    GET_USERS,
    USER_DELETE,
    USER_MODIFIED, USERS_GET_ERROR,
    USERS_RECEIVED
} from "../actions/users.actions";

const initialState = {
    usersList: [],
    getSuccess: true,
    isLoading: false
};

export function usersReducer(state = initialState, action) {
    switch(action.type){
        case GET_USERS:
            return{
                ...state,
                isLoading: true,
                getSuccess: true
            };
        case USERS_RECEIVED:
            return{
                ...state,
                usersList: action.payload,
                isLoading: false,
                getSuccess: true
            };
        case USERS_GET_ERROR:
            return {
                ...state,
                getSuccess: false,
                isLoading: false
            };
        case USER_MODIFIED:
            return{
                ...state,
                usersList: [
                    ...state.usersList.slice(0, action.payload.index),
                    action.payload.user,
                    ...state.usersList.slice(action.payload.index + 1)
                ],
            };
        case USER_DELETE:
            return {
                ...state,
                usersList: state.usersList.filter((item, index) => {
                    return index !== action.payload.index
                }),
            };
        default:
            return {
                ...state
            }
    }
}