export const GET_USERS = "GET_USERS";
export const USERS_RECEIVED = "USERS_RECEIVED";
export const USERS_GET_ERROR = "USERS";
export const USER_MODIFIED = "USER_MODIFIED";
export const USER_DELETE = "USER_DELETE";

export const getUsers = () => {
    return {
        type: GET_USERS
    }
};

export const usersReceived = (users) => {
    return {
        type: USERS_RECEIVED,
        payload: users
    }
};

export const usersGetError = () => {
    return {
        type: USERS_GET_ERROR
    }
};

export const userModified = (index, user) => {
    return {
        type: USER_MODIFIED,
        payload: {
            index,
            user: user
        }
    }
};

export const userDelete = (index) => {
    return {
        type: USER_DELETE,
        payload: {
            index
        }
    }
};