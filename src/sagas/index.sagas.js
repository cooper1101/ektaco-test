import {fork, all} from 'redux-saga/effects';
import * as userSagas from './users.saga';

export function* rootSaga(){
    yield all([
        ...Object.values(userSagas)
    ].map(fork));
}