import {API_URL_BASE, GET_USER_AMOUNT} from "../global.constats";
import {put, takeLatest} from "redux-saga/effects";
import {GET_USERS, usersGetError, usersReceived} from "../actions/users.actions";

function* getUsersAsync(){
    const apiResponse = yield fetch(`${API_URL_BASE}?amount=${GET_USER_AMOUNT}`)
        .then(data => {
            return data.json();
        }).catch(error => {
            console.log(error); //ideally put a custom logger here that logs excpetion to DB or Splunk
        });

    if(apiResponse && apiResponse.length > 0)
        yield put(usersReceived(apiResponse));
    else
        yield put(usersGetError())

}

export function* getUsersWatch(){
    yield takeLatest(GET_USERS, getUsersAsync);
}