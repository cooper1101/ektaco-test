import React, {Component, Fragment} from 'react';
import UserListContainer from './views/userlist.container';
import EditUserContainer from './views/edituser.container';
import './styles/app.css';
import {Route, Switch} from "react-router-dom";

class App extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <h1>Ektaco Test</h1>
                    <h4>User List</h4>
                </header>
                <Switch>
                    <Route exact path="/" component={UserListContainer}/>
                    <Route path="/:index" component={EditUserContainer}/>
                </Switch>
            </Fragment>
        );
    }
}

export default App;
