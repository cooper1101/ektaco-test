import React from 'react';
import {getUsers, userDelete} from "../actions/users.actions";
import {connect} from "react-redux";
import {UserTable} from "./components/userlist.table";
import {GetUsersButton} from "./components/getusers.button";
import {Alert, Grid, Row} from "react-bootstrap";
import {SpinnerLoader} from "./components/spinner.loader";

const UserListContainer = (props) => {

    const handleEditUser = (index) => {
        props.history.push(`/${index}`);
    };

    const handleGetUsers = () => {
        props.getUsers();
    };

    const handleUserDelete = (index) => {
        props.deleteUser(parseInt(index, 10));
    };

    const handleRenderConditions = () => {
        if(!props.getSuccess){
            return(
                <Alert bsStyle="danger">
                    Error fetching user data.
                </Alert>
            )
        }
        if(props.isLoading){
            return(
                <SpinnerLoader isLoading={props.isLoading}/>
            );
        }
        return(
            <UserTable
                isLoading={props.isLoading}
                users={props.usersList}
                onUserClick={handleEditUser}
                onUserDelete={handleUserDelete}
            />
        )
    };

    return(
        <Grid>
            <Row className="center-btn-row">
                <GetUsersButton
                    onGetUsers={handleGetUsers}
                />
            </Row>
            <Row>
                {handleRenderConditions()}
            </Row>
        </Grid>
    )
};

const mapStateToProps = (state) => {
    return {
        usersList: state.users.usersList,
        isLoading: state.users.isLoading,
        getSuccess: state.users.getSuccess
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getUsers: () => {
            dispatch(getUsers());
        },
        deleteUser: (index) => {
            dispatch(userDelete(index));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(UserListContainer);