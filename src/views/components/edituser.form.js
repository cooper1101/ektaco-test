import React from "react";
import {Button, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";
import {ALLOWED_GENDERS} from "../../global.constats";

export const EditUserForm = ({userData, onInputChange, onFormSubmit}) => {
    return(
        <Form onSubmit={onFormSubmit} className="form-edit-user">
            <FormGroup controlId="name">
                <ControlLabel>Name</ControlLabel>
                <FormControl
                    type="text"
                    value={userData.name}
                    name="name"
                    placeholder="Enter text"
                    onChange={onInputChange}
                />
                <FormControl.Feedback />
            </FormGroup>
            <FormGroup controlId="surname">
                <ControlLabel>Surname</ControlLabel>
                <FormControl
                    type="text"
                    value={userData.surname}
                    name="surname"
                    onChange={onInputChange}
                />
                <FormControl.Feedback />
            </FormGroup>
            <FormGroup controlId="gender">
                <ControlLabel>Gender</ControlLabel>
                <FormControl
                    onChange={onInputChange}
                    name="gender"
                    componentClass="select"
                    placeholder="select"
                    defaultValue={userData.gender}
                >
                    {ALLOWED_GENDERS.map((gender, index) => {
                        return(
                            <option key={index} value={gender}>
                                {gender}
                            </option>
                        );
                    })}
                </FormControl>
            </FormGroup>
            <FormGroup controlId="name">
                <ControlLabel>Region</ControlLabel>
                <FormControl
                    type="text"
                    value={userData.region}
                    name="region"
                    onChange={onInputChange}
                />
                <FormControl.Feedback />
            </FormGroup>
            <Button
                bsStyle="success"
                type="submit">
                Save
            </Button>
        </Form>
    );
};