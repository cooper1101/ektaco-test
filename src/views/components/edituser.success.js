import React from 'react';
import {Alert} from "react-bootstrap";

export const SuccessMessage = ({isSaved}) => {
    if(isSaved)
        return(
            <Alert bsStyle="success">
                <strong>Success!</strong> User has been saved.
            </Alert>
        );
    return null;
};