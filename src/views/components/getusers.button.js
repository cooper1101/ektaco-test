import React from 'react';
import {Button} from "react-bootstrap";

export const GetUsersButton = ({onGetUsers}) => {
    return(
        <Button
            id="GetUsersBtn"
            bsStyle="primary"
            bsSize="large"
            onClick={onGetUsers}
        >
            Get Users
        </Button>
    );
};