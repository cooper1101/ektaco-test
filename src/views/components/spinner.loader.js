import React from 'react';
import {LOADER_COLOR, LOADER_SIZE} from "../../global.constats";
import {ClipLoader} from "react-spinners";
import '../../styles/loader.css';

export const SpinnerLoader = ({isLoading}) => {
    return(
        <div className="loader-container">
            <ClipLoader
                color={LOADER_COLOR}
                loading={isLoading}
                size={LOADER_SIZE}
            />
        </div>
    );
};