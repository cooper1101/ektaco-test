import React from 'react';
import {Button, Col, Grid, Row, Well} from "react-bootstrap";

export const UserTable = ({users, onUserClick, onUserDelete}) => {
    return(
        <Grid>
            <Row>
                {
                    users.map((user, index) => {
                        return(
                            <Col className={"col-padding-custom"} xs={12} sm={12} md={6} lg={4} key={index}>
                                <Well>
                                    <Row>
                                    <Col xs={12} sm={12} md={12} lg={12}>
                                        <h4>{user.name} {user.surname}</h4>
                                    </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={4} sm={4} md={3} lg={2}>
                                            <Button bsStyle="warning" onClick={() => onUserClick(index)}>Edit</Button>
                                        </Col>
                                        <Col xs={4} sm={4} md={3} lg={2}>
                                            <Button bsStyle="danger" onClick={() => onUserDelete(index)}>Delete</Button>
                                        </Col>
                                    </Row>
                                </Well>
                            </Col>
                        )
                    })
                }
            </Row>
        </Grid>
    )
};