import React, {Component} from 'react';
import {connect} from "react-redux";
import {userModified} from "../actions/users.actions";
import {Button, Grid, Row} from "react-bootstrap";
import {EditUserForm} from "./components/edituser.form";
import {SuccessMessage} from "./components/edituser.success";

class EditUserContainer extends Component {
    constructor(props){
        super(props);
        this._index = parseInt(props.match.params.index, 10);
        this._user = props.users[this._index];

        if(this._user){
            this.state = {
                name: this._user.name,
                surname: this._user.surname,
                gender: this._user.gender,
                region: this._user.region,
                isSaved: false
            }
        }
        if(!this._user) props.history.push('');
    }

    handleFieldChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
            isSaved: false
        })
    };

    handleFormSubmit = (e) => {
        e.preventDefault();
        const user = {
            name: this.state.name,
            surname: this.state.surname,
            gender: this.state.gender,
            region: this.state.region
        };
        this.props.updateUser(this._index, user);
        this.setState({isSaved: true});
    };

    handleGoBack = () => {
        this.props.history.goBack();
    };

    render(){
        if(!this._user) return null;

        return(
            <Grid className="align-center">
                <Row className="edit-form-row">
                    <EditUserForm
                        userData={this.state}
                        onInputChange={this.handleFieldChange}
                        onFormSubmit={this.handleFormSubmit}
                    />
                    <div className="validation-wrapper">
                        <SuccessMessage isSaved={this.state.isSaved}/>
                    </div>
                </Row>
                <hr/>
                <Row className="row-margin-top">
                    <Button
                        bsStyle="primary"
                        onClick={this.handleGoBack}>
                        Go Back
                    </Button>
                </Row>
            </Grid>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        users: state.users.usersList
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateUser: (index, user) => {
            dispatch(userModified(index, user));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(EditUserContainer)